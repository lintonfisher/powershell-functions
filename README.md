# PowerShell Functions

This repository is a collection of timesaving PowerShell Scripts I have written.

## Installation

1. Download the latest [release](https://gitlab.com/lintonfisher/powershell-functions/-/releases).
1. Extract zip to `C:\Users\USERNAME\WindowsPowerShell\`

## Usage

The `Profile.ps1` should run whenever you start a PowerShell window, and all the functions should be loaded automatically.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.