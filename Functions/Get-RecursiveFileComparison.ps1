Function Get-RecursiveFileComparison {
    Param (
        [Parameter(Mandatory=$True)]
        [ValidateScript({Test-Path $_})]
        [String]
        $SrcPath,
        [Parameter(Mandatory=$True)]
        [ValidateScript({Test-Path $_})]
        [String]
        $DstPath,
        [Switch]
        $IgnoreFolders,
        [Switch]
        $IgnoreFileExtensions,
        [String[]]
        $ExcludedFiles
    )

    # If -IgnoreFolders is passed, do not include Directories in the
    # list of Source files and Destination files.
    If ($IgnoreFolders) {
        [System.Collections.ArrayList]$SrcItems = Get-ChildItem -Path $SrcPath -Recurse | Where-Object -Property Attributes -NE -Value Directory
        [System.Collections.ArrayList]$DstItems = Get-ChildItem -Path $DstPath -Recurse | Where-Object -Property Attributes -NE -Value Directory
    } Else {
        [System.Collections.ArrayList]$SrcItems = Get-ChildItem -Path $SrcPath -Recurse
        [System.Collections.ArrayList]$DstItems = Get-ChildItem -Path $DstPath -Recurse
    }

    # Remove the files passed in the -IgnoredFiles parameter
    # from the list of Source files and Destination files.
    If ($ExcludedFiles.Length -gt 0) {
        $ExcludedFiles | ForEach-Object {
            $SrcItems = $SrcItems | Where-Object -Property Name -NotLike -Value $_
            $DstItems = $DstItems | Where-Object -Property Name -NotLike -Value $_
        }
    }

    # If -IgnoreFileExtensions is passed, remove everything after the first '.'
    If ($IgnoreFileExtensions) {
        $SrcList = @()
        $SrcItems | ForEach-Object {
            $SrcList += $_.FullName.Replace($SrcPath, "").Split(".") | Select-Object -First 1
        }
        $DstList = @()
        $DstItems | ForEach-Object {
            $DstList += $_.FullName.Replace($DstPath, "").Split(".") | Select-Object -First 1
        }
    } Else {
        $SrcList = $SrcItems.FullName.Replace($SrcPath, "")
        $DstList = $DstItems.FullName.Replace($DstPath, "")
    }

    $MissingFiles = @()
    ForEach ($SrcFile in $SrcList) {
        If (!$DstList.Contains($SrcFile)) {
            $MissingFile = [PSCustomObject]@{
                File = $SrcFile
                Side = "Src"
            }
            $MissingFiles += $MissingFile
        }
    }
    ForEach ($DstFile in $DstList) {
        If (!$SrcList.Contains($DstFile)) {
            $MissingFile = [PSCustomObject]@{
                File = $DstFile
                Side = "Dst"
            }
            $MissingFiles += $MissingFile
        }
    }

    # Return the missing files
    $MissingFiles
}