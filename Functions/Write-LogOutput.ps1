# Function to print to the screen with the current time and severity level
Function Write-LogOutput {
    Param(
        [Parameter(Mandatory = $True)]
        [ValidateSet("INFO", "WARNING", "ERROR", "DEBUG", "EXEC")]
        $Sev,
        [Parameter(Mandatory = $True)]
        $Message
    )

    $Timestamp = Get-Date -Format "HH:mm:ss"
    Switch ($Sev) {
        "INFO" { $Colour = "Green"; }
        "WARNING" { $Colour = "Yellow"; }
        "ERROR" { $Colour = "Red"; }
        "DEBUG" { $Colour = "Magenta"; }
        "EXEC" { $Colour = "Cyan"; }
    }

    # Only print DEBUG output if -DebugMode was passed
    If ((($Sev -eq "DEBUG") -and $DebugMode) -or ($Sev -ne "DEBUG")) {
        Write-Host -ForegroundColor $Colour -Object "$Timestamp`t$Sev`t$Message"
    }
}