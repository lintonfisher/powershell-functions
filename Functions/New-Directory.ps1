# Function to create a directory if it doesn't exist
Function New-Directory {
    Param (
        [Parameter(Mandatory = $True)]
        $Path
    )

    If ($(Test-Path -Path $Path) -eq $False) {
        New-Item -Path $Path -ItemType Directory
        Write-LogOutput DEBUG "Created directory $Path"
    }
}