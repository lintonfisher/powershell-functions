# Custom multi-line prompt
Function Prompt {
    "[$($executionContext.SessionState.Path.CurrentLocation)]`n> "
}